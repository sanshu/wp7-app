﻿
namespace NextApp. WP7. Cache. Base
{
	/// <summary>
	/// 这样的任务仅仅只能作用于 Http Get 请求
	/// </summary>
	public class AsyncMissionBase
	{
		/// <summary>
		/// Http Get 操作的uri
		/// </summary>
		public string queryStringWithUrl { get; set; }
	}
}
