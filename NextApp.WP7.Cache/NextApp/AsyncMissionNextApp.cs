﻿using NextApp. WP7. Cache. Base;

namespace NextApp. WP7. Cache. NextApp
{
	public sealed class AsyncMissionNextApp : AsyncMissionBase
	{
		public AsyncMissionNextAppType Type = AsyncMissionNextAppType. None;
	}

	public enum AsyncMissionNextAppType
	{
		None,
 		Post_Detail,
		Post_List,
		Comment_List,
	}
}
