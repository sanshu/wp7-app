﻿using System;
using System. Collections. Generic;
using System. Linq;
using System. Text;

namespace TechDemoConsole
{
    class Program
    {
        static void Main(string[ ] args)
        {
            Process( );
        }

        static void Process( )
        {
            A[ ] datas1 = new A[ ]
            {
                new A { a = 1, b = 10 },
                new A{a = 2, b = 20},
                new A{a = 3, b= 30},
                new A{a=4, b=40},
                new A{a = 5 , b = 50},
            };

            A[ ] datas2 = new A[ ]
            {
                new A { a = 1, b = 10 },
                new A{a = 3, b= 30},
                new A{a = 5 , b = 50},
                 new A{a = 7, b= 70},
                new A{a = 9, b = 90},
            };

            var datas3 = datas1. Union(datas2, new ACompare( )). ToList( );
            //datas3 = datas1. Union(datas2). ToList( );
            datas3. ForEach(d => Console. WriteLine(d. ToString( )));
        }
    }

    public class A
    {
        public int a;
        public int b;
        public override string ToString( )
        {
            return b. ToString( );
        }
    }

    public class ACompare : IEqualityComparer<A>
    {
        public bool Equals(A x, A y)
        {
            return x. a == y. a;
        }
        public int GetHashCode(A obj)
        {
            //return obj. GetHashCode( );
            return base. GetHashCode( );
        }
    }

}
