﻿#pragma checksum "D:\Makingware\Makingware.App.WordPress.WP7\NextApp.WP7\View\Controls\About.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "6D03A8993F57EFEF2CA198B965AEBE88"
//------------------------------------------------------------------------------
// <auto-generated>
//     這段程式碼是由工具產生的。
//     執行階段版本:4.0.30319.239
//
//     對這個檔案所做的變更可能會造成錯誤的行為，而且如果重新產生程式碼，
//     變更將會遺失。
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace NextApp.WP7.View.Controls {
    
    
    public partial class About : System.Windows.Controls.UserControl {
        
        internal System.Windows.Controls.Grid LayoutRoot;
        
        internal System.Windows.Controls.HyperlinkButton link_Website;
        
        internal System.Windows.Controls.HyperlinkButton link_Support;
        
        internal System.Windows.Controls.TextBlock tblock_Description;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/NextApp.WP7;component/View/Controls/About.xaml", System.UriKind.Relative));
            this.LayoutRoot = ((System.Windows.Controls.Grid)(this.FindName("LayoutRoot")));
            this.link_Website = ((System.Windows.Controls.HyperlinkButton)(this.FindName("link_Website")));
            this.link_Support = ((System.Windows.Controls.HyperlinkButton)(this.FindName("link_Support")));
            this.tblock_Description = ((System.Windows.Controls.TextBlock)(this.FindName("tblock_Description")));
        }
    }
}

