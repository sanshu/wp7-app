﻿using System;
using Coding4Fun. Phone. Controls;
using Microsoft. Phone. Controls;
using NextApp. WP7. AppConfig;
using NextApp. WP7. Models;
using NextApp. WP7. View. Controls;
using WP7_ControlsLib. Controls;
using System. Windows;
using NextApp. WP7. Util;

namespace NextApp. WP7
{
    public partial class MainPage : PhoneApplicationPage
    {
        #region Initialize
        public MainPage( )
        {
            InitializeComponent( );
			this. PrepareToOrientation( );
            //获取标题
            this. pivot. Title = Config. Instance. blog_title;
            //一旦API地址全部获取后 则立即获取数据
            /*
             * 如果是 桌面上的 分类图标或者文章图标点击进来 则有下面这个方法会无效
             */
            NextAppSingleton. Instance. OnGetApiCompleted += (s, e) =>
                {
                    //标志可以进行了
                    this. IsEnabled = true;
                    this. catalog_list. Reload( );
                    this. posts_list. Reload( );
                    this. comment_list. Reload( );
                };
            //处理 MainPage 的进度条显示
            EventSingleton. Instance. OnMainPageLoading += (s1, e1) =>
                {
                    ProgressBarHelper. IsDisplayIndeterminateProgressBar(( bool )e1. Tag, "加载中");
                };
            //当删除评论的时候需要刷新评论
            EventSingleton. Instance. OnRefreshComments += (s1, e1) =>
                {
                    //如果需要刷新的评论控件就是自身
                    if ( (s1 as CommentList) == this.comment_list )
                    {
						//DataSingleton. Instance. ClearComments(0);
                        this. comment_list. Reload(true, 0, 0, true);
                    }
                };
        }

        protected override void OnNavigatedTo(System. Windows. Navigation. NavigationEventArgs e)
        {
            if ( NextAppSingleton. Instance. IsReady )
            {
                //标志可以进行了
                this. IsEnabled = true;
                this. catalog_list. Reload( );
                this. posts_list. Reload( );
                this. comment_list. Reload( );
            }
            //检测是否登陆来判断 ApplicationBar
            Util. Tool. CheckAppBar(this);

            base. OnNavigatedTo(e);
        }

        #endregion

        #region Back 按键处理
        protected override void OnBackKeyPress(System. ComponentModel. CancelEventArgs e)
        {
            if ( this. NavigationService. CanGoBack )
            {
                this. NavigationService. GoBack( );
            }
            else
            {
                if ( MessageBox. Show("是否确认退出程序", "温馨提示", MessageBoxButton. OKCancel) == MessageBoxResult. OK )
                {
                    //退出处理
                }
                else
                {
                    e. Cancel = true;
                }
            }

            base. OnBackKeyPress(e);
        }

        #endregion

        #region 底部的 ApplicationBar 相关事件
        private void menuItem_About_Click(object sender, EventArgs e)
        {
            //弹出关于我们提示框
            AboutPrompt about = new AboutPrompt
            {
                Title = Config. Instance. about_title,
				Footer = /*Config. Instance. about_copyright*/  "Powered by NextApp.cn",
                VersionNumber = /*Config. Instance. about_version*/ "版本 1.0",
                Body = new About( ),
            };
            about. Show( );
        }

        private void icon_Logout_Click(object sender, EventArgs e)
        {
            Util. Tool. Logout( );
            Util. Tool. CheckAppBar(this);
        }

        /// <summary>
        /// 刷新按钮点击事件
        /// </summary>
        private void icon_Reload_Click(object sender, EventArgs e)
        {
            switch ( this.pivot.SelectedIndex )
            {
                case 0:
                    this. catalog_list. Reload( );
                    break;
                case 1:
					//DataSingleton. Instance. ClearPosts(0);
                    this. posts_list. Reload(true, 0, true);
                    break;
                case 2:
					//DataSingleton. Instance. ClearComments(0);
                    this. comment_list. Reload(true, 0, 0, true);
                    break;
                default:
                    System. Diagnostics. Debug. WriteLine("点击刷新按钮后发生错误");
                    break;
            }
        }

        private void icon_Login_Click(object sender, EventArgs e)
        {
            Util. Tool. Login( );
        }

        private void icon_NewPost_Click(object sender, EventArgs e)
        {
            Util. Tool. AddPost_Click( );
        }
        #endregion
    }
}