﻿using System;
using NextApp. WP7. UnionLib;
using Control = NextApp. WP7. View. Controls;

namespace NextApp. WP7.Models
{
    /// <summary>
    /// 本单例对象 主要用于整个程序的事件通知
    /// </summary>
    public sealed class EventSingleton
    {
        #region Singleton
        private static readonly EventSingleton instance = new EventSingleton( );
        public static EventSingleton Instance { get { return instance; } }
        private EventSingleton( ) { }
        #endregion

        #region 拉取文章后产生事件通知 启动轮询搜索
        public event EventHandler<TagEventArgs> OnGetPostsCompleted;
        public void RaiseOnGetPosts( RenewBlogs renewBlogs )
        {
            if ( this.OnGetPostsCompleted != null )
            {
                this. OnGetPostsCompleted(this, new TagEventArgs { Tag = renewBlogs });
            }
        }

        #endregion

        #region 通知 MainPage 的加载进度条运行
        public event EventHandler<TagEventArgs> OnMainPageLoading;
        public void RaiseOnMainPageLoading(bool isMainPageLoading)
        {
            if ( this.OnMainPageLoading != null )
            {
                this. OnMainPageLoading(this, new TagEventArgs { Tag = isMainPageLoading });
            }
        }

        #endregion

        #region 删除文章以及删除评论以后要通知重新刷新
        public event EventHandler OnRefreshComments;
        public void RaiseOnRefreshComments( Control.CommentList commentListControl )
        {
            if ( this.OnRefreshComments != null )
            {
                this. OnRefreshComments(commentListControl, null);
            }
        }

        #endregion
    }
}
