﻿using System;
using System. Windows;
using Microsoft. Phone. Controls;
using NextApp. WP7. Models;
using NextApp. WP7. Weibo. ViewModel;
using TencentWeiboSDK;

namespace NextApp. WP7. Weibo. View
{
    public partial class TencentOAuth : PhoneApplicationPage
    {
        public TencentOAuth( )
        {
            InitializeComponent( );
            // 配置 AppKey 和 AppSecret
            OAuthConfigruation. APP_KEY = /*"801075626"*/ "801080291";
            OAuthConfigruation. APP_SECRET = /*"d0e1e5bc4f84879972761dfbd7b53796"*/ "3c1b59290717cf27185a38f6cd7acc49";
            OAuthConfigruation. ClearAccessToken( );
            // 若你希望自己管理 AccessToken 则设置此参数.
            //OAuthConfigruation.IfSaveAccessToken = false;
            this. Loaded += new RoutedEventHandler(TencentOAuth_Loaded);
        }

        void TencentOAuth_Loaded(object sender, RoutedEventArgs e)
        {
            // 开始进行 OAuth 授权.
            this. oAuthLoginBrowser. OAuthLogin((callback) =>
            {
                // 若已获得 AccessToken 则跳转到 TimelineView 页面
                // 注意： 若 OAuthConfigruation.IfSaveAccessToken 属性为 False，则需要在此处保存用户的 AccessToken(callback.Data) 以便下次使用.
                if ( callback. Succeed )
                {
                    //直接分享
                    this. Share( );
                }
            });
        }

        private void Share( )
        {
            PostNewViewModel vm = new PostNewViewModel( );
            vm. Text = string. Format("{0}  {1}", DataSingleton. Instance. postToShare. title, DataSingleton. Instance. postToShare. url);
            vm. Post(( ) =>
            {
                Dispatcher. BeginInvoke(( ) =>
                    {
                        this. NavigationService. Navigate(new Uri(string. Format("/View/PostDetail.xaml?post={0}&portal={1}", 
                                        DataSingleton. Instance. postToShare. id, DataSingleton.Instance.GoToSharePageType), UriKind. Relative));
                    });
            });
        }


    }
}