﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace 测试_osmobile_API.Model
{
    public sealed class comment
    {
        public int id { get; set; }
        public int post { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string url { get; set; }
        public string body { get; set; }
    }
}
