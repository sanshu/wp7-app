﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Xml.Linq;
using 测试_osmobile_API.Config;
using 测试_osmobile_API.Model;
using WP7_WebLib.HttpPost;
using System.IO.IsolatedStorage;

namespace 测试_osmobile_API.API_Test
{
    public partial class LoginValidate_Test : UserControl
    {
        public LoginValidate_Test()
        {
            InitializeComponent();
        }

        private void btn_Login_Click(object sender, RoutedEventArgs e)
        {
            PostClient client = new PostClient(new Dictionary<string, object>
                {
                    {"username", txt_UserName.Text.Trim()},
                    {"pwd", txt_Pwd.Text.Trim()},
                    {"keep_login", (bool)check_RemeberMe.IsChecked ? "1": "0"},
                });
            client. OnGetCookie += (cookie) =>
                {
                    CookieUtil. Instance. Cookie_All = cookie;
                };
            client.DownloadStringCompleted += (s, e1) =>
                {
                    if (e1.Error != null)
                    {
                        System.Diagnostics.Debug.WriteLine(e1.Error.Message);
                        return;
                    }
                    XElement x = XElement.Parse(e1.Result).Element("result");
                    error _error = new error
                    {
                        errorCode = x.Element("errorCode").Value.ToInt32(),
                        errorMessage = x.Element("errorMessage").Value,
                    };
                    //分析错误代码
                    System.Diagnostics.Debug.WriteLine(_error);
                    CookieUtil.Instance.Login_UserName = txt_UserName.Text.Trim();
                    CookieUtil.Instance.Login_Password = txt_Pwd.Text.Trim();
                    CookieUtil.Instance.Login_RemeberMe = (bool)check_RemeberMe.IsChecked;
                    MessageBox.Show(_error.errorMessage, _error.errorCode.ToString(), MessageBoxButton.OK);
                };
            client.DownloadStringAsync(new Uri(AppConfig.Instance.GetApi("osmobile=auth"), UriKind.Absolute));
        }
    }
}
