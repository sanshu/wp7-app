﻿using System.IO.IsolatedStorage;

namespace 测试_osmobile_API.Config
{
    public sealed class CookieUtil
    {
        #region Singleton
        private static readonly CookieUtil instance = new CookieUtil();
        public static CookieUtil Instance { get { return instance; } }
        private CookieUtil() { }
        #endregion

        public string Login_UserName
        {
            get 
            {
                return IsolatedStorageSettings.ApplicationSettings.Contains("Login_UserName") ?
                    IsolatedStorageSettings.ApplicationSettings["Login_UserName"].ToString() : string.Empty;
            }
            set 
            {
                IsolatedStorageSettings.ApplicationSettings["Login_UserName"] = value;
                IsolatedStorageSettings.ApplicationSettings.Save();
            }
        }

        public string Login_Password
        {
            get
            {
                return IsolatedStorageSettings.ApplicationSettings.Contains("Login_Password") ?
                    IsolatedStorageSettings.ApplicationSettings["Login_Password"].ToString() : string.Empty;
            }
            set
            {
                IsolatedStorageSettings.ApplicationSettings["Login_Password"] = value;
                IsolatedStorageSettings.ApplicationSettings.Save();
            }
        }

        public bool Login_RemeberMe
        {
            get
            {
                return IsolatedStorageSettings.ApplicationSettings.Contains("Login_RemeberMe") ?
                    (bool)IsolatedStorageSettings.ApplicationSettings["Login_RemeberMe"] : false;
            }
            set
            {
                IsolatedStorageSettings.ApplicationSettings["Login_RemeberMe"] = value;
                IsolatedStorageSettings.ApplicationSettings.Save();
            }
        }

        public string Cookie_All
        {
            get 
            {
                return IsolatedStorageSettings. ApplicationSettings. Contains("Cookie_All") ?
                    IsolatedStorageSettings. ApplicationSettings[ "Cookie_All" ]. ToString( ) : null;
            }
            set 
            {
                IsolatedStorageSettings. ApplicationSettings[ "Cookie_All" ] = value;
                IsolatedStorageSettings. ApplicationSettings. Save( );
            }
        }
    }
}
