﻿using System;

namespace NextApp. WP7. Models
{
    public sealed class Comment
    {
        public int id { get; set; }
        public int post { get; set; }
        public string name { get;  set; }
        public string email { get; set; }
        public string url { get; set; }
        public string body { get; set; }
        public string pubDate { get; set; }
        public string pubDate_Date
        {
            get { return this. pubDate_Format. ToString("yyy-MM-dd hh:mm"); }
        }
        /// <summary>
        /// 评论发表时间的DateTime格式
        /// </summary>
        public DateTime pubDate_Format
        {
            get { return this. pubDate. ToDateTime( ); }
            set { ; }
        }
    }
}
