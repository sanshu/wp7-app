﻿
namespace NextApp. WP7. Models
{
    /// <summary>
    /// 文章类别
    /// </summary>
    public sealed class Catalog
    {
        /// <summary>
        /// 分类ID
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// 分类简称
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// 分类具体描述
        /// </summary>
        public string description { get; set; }
    }
}
