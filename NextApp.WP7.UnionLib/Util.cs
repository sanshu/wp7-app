﻿using System;

namespace NextApp. WP7. Models
{
    public static class Util
    {
        public static DateTime ToDateTime(this string dateTime_str)
        {
            //字符串格式为 2011-12-07 18:29:54
            //判断是否有空格
            if ( dateTime_str. Contains(" ") )
            {
                string[ ] partials = dateTime_str. Split(' ');
                string[ ] days = partials[ 0 ]. Split('-');
                string[ ] times = partials[ 1 ]. Split(':');
                return new DateTime(days[ 0 ]. ToInt32( ), days[ 1 ]. ToInt32( ), days[ 2 ]. ToInt32( ), times[ 0 ]. ToInt32( ), times[ 1 ]. ToInt32( ), times[ 2 ]. ToInt32( ));
            }
            else
            {
                string[ ] days = dateTime_str. Split('-');
                return new DateTime(days[ 0 ]. ToInt32( ), days[ 1 ]. ToInt32( ), days[ 2 ]. ToInt32( ));
            }
        }
    }
}
