﻿using System. Windows;
using Microsoft. Phone. Controls;
using Microsoft. Phone. Shell;
using System;

namespace TechDemo
{
    public partial class MainPage : PhoneApplicationPage
    {
        // 建構函式
        public MainPage( )
        {
            InitializeComponent( );

            this. ApplicationBar = Application. Current. Resources[ "appbar_1" ] as ApplicationBar;
        }

        private void btn_ChangeAppBar1_Click(object sender, RoutedEventArgs e)
        {
            this. ApplicationBar = Application. Current. Resources[ "appbar_2" ] as ApplicationBar;
        }

        private void btn_ChangeAppBar2_Click(object sender, RoutedEventArgs e)
        {
            this. ApplicationBar = Application. Current. Resources[ "appbar_1" ] as ApplicationBar;
        }

        private void btn_ToProgressPage_Click(object sender, RoutedEventArgs e)
        {
            this. NavigationService. Navigate(new Uri("/ProgressPage.xaml", UriKind. Relative));
        }
    }
}