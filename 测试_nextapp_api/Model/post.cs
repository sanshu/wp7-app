﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace 测试_osmobile_API.Model
{
    public sealed class post
    {
        public int id { get; set; }
        public int[] catalog { get; set; }
        public string title { get; set; }
        public string url { get; set; }
        public string body { get; set; }
        public string[] tags { get; set; }
        public string author { get; set; }
        public string pubDate { get; set; }
        //时间日期的问题 待办 主要是格式方面的问题
        public int commentCount { get; set; }
        //关联的帖子
        public relativePost[] relativePosts { get; set; }
        //主要用于 post-list API返回的文章集合
        public string outline { get; set; }
        public int authorId { get; set; }
        public string catalogName { get; set; }
    }
}
