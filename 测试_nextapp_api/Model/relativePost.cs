﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace 测试_osmobile_API.Model
{
    public sealed class relativePost
    {
        public int id { get; set; }
        public string title { get; set; }
        public string author { get; set; }
        public string pubDate { get; set; }
    }
}
