﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Xml.Linq;
using 测试_osmobile_API.Config;
using 测试_osmobile_API.Model;

namespace 测试_osmobile_API.API_Test
{
    public partial class Catelog_Test : UserControl
    {
        public Catelog_Test()
        {
            InitializeComponent();
        }

        private void btn_Catelog_Test_Click(object sender, RoutedEventArgs e)
        {
            WebClient client = new WebClient();
            client.DownloadStringCompleted += (s, e1) =>
                {
                    if (e1.Error != null)
                    {
                        System.Diagnostics.Debug.WriteLine("获取 catelog 出错: {0}", e1.Error.Message);
                        return;
                    }
                    XElement osmobile = XElement.Parse(e1.Result);
                    catalog[] catalogs = osmobile.Elements("catalogs").Elements("catalog").Select(
                        c =>
                        {
                            Dictionary<string, string> attributes = c.Attributes().ToDictionary(a => a.Name.LocalName, a => a.Value);
                            return new catalog
                            {
                                description = attributes["description"],
                                id = attributes["id"].ToInt32(),
                                name = attributes["name"],
                            };
                        }
                        ).ToArray();
                    //测试成功 获取了所有博客分类
                    System.Diagnostics.Debug.WriteLine(catalogs);
                };
            client.DownloadStringAsync(new Uri(AppConfig.Instance.GetApi("nextapp=catalog&action=list"), UriKind.Absolute));
        }
    }
}
