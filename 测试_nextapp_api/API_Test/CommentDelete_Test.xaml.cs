﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Xml.Linq;
using 测试_osmobile_API.Config;
using 测试_osmobile_API.Model;
using WP7_WebLib.HttpPost;

namespace 测试_osmobile_API.API_Test
{
    public partial class CommentDelete_Test : UserControl
    {
        public CommentDelete_Test()
        {
            InitializeComponent();
        }

        private void btn_DelComment_Test_Click(object sender, RoutedEventArgs e)
        {
            #region 删除评论方面一直使用 http Post 出错
            //PostClient client = new PostClient(new Dictionary<string, object>
            //{
            //    {"comment", txt_CommentID.Text.Trim().ToInt32()},
            //});
            //client.DownloadStringCompleted += (s, e1) =>
            //    {
            //        if (e1.Error != null)
            //        {
            //            System.Diagnostics.Debug.WriteLine("删除评论出错: {0}", e1.Error.Message);
            //            return;
            //        }
            //        XElement x = XElement.Parse(e1.Result).Element("result");
            //        error _error = new error
            //        {
            //            errorCode = x.Element("errorCode").Value.ToInt32(),
            //            errorMessage = x.Element("errorMessage").Value,
            //        };
            //        //获取返回的结果信息
            //        System.Diagnostics.Debug.WriteLine(_error);
            //        MessageBox.Show(_error.errorMessage, _error.errorCode.ToString(), MessageBoxButton.OK);
            //    };
            //client. DownloadStringAsync(new Uri(AppConfig. Instance. GetApi("osmobile=comment&action=delete"), UriKind.Absolute), CookieUtil. Instance. Cookie_All);
            #endregion
            //换成 http Get 就成功了
            this. HttpGet( );
        }

        private void HttpGet( )
        {
            WebClient client = new WebClient( );
            client. DownloadStringCompleted += (s, e1) =>
                {
                    if ( e1.Error != null )
                    {
                        System. Diagnostics. Debug. WriteLine("删除评论失败: {0}", e1. Error. Message);
                        return;
                    }
                    XElement osmobile = XElement. Parse(e1. Result);
                    error _error = new error
                    {
                        errorCode = osmobile. Element("result"). Element("errorCode"). Value. ToInt32( ),
                        errorMessage = osmobile. Element("result"). Element("errorMessage"). Value,
                    };
                    MessageBox. Show(_error. errorMessage, _error. errorCode. ToString( ), MessageBoxButton. OKCancel);
                };
            client. Headers[ "Cookie" ] = CookieUtil. Instance. Cookie_All;
            client. DownloadStringAsync(new Uri(AppConfig. Instance. GetApi(string. Format("nextapp=comment&action=delete&comment={0}", txt_CommentID. Text. Trim( ). ToInt32( ))), 
                UriKind. Absolute));
        }
    }
}
