﻿using System;
using System. Windows;
using Coding4Fun. Phone. Controls;
using Microsoft. Phone. Controls;
using NextApp. WP7. AppConfig;
using NextApp. WP7. Models;
using NextApp. WP7. Util;
using NextApp. WP7. View. Controls;
using WP7_ControlsLib. Controls;

namespace NextApp. WP7
{
    public partial class MainPage : PhoneApplicationPage
    {
        #region Initialize
        public MainPage( )
        {
            InitializeComponent( );
			this. PrepareToOrientation( );
            //获取标题
            this. pivot. Title = Config. Instance. blog_title;
            //一旦API地址全部获取后 则立即获取数据
            /*
             * 如果是 桌面上的 分类图标或者文章图标点击进来 则有下面这个方法会无效
             */
            NextAppSingleton. Instance. OnGetApiCompleted += (s, e) =>
                {
                    //标志可以进行了
                    this. IsEnabled = true;
                    this. catalog_list. Reload( );
                    this. posts_list. Reload( );
                    this. comment_list. Reload( );
                };
            //处理 MainPage 的进度条显示
            EventSingleton. Instance. OnMainPageLoading += (s1, e1) =>
                {
					ProgressBarHelper. IsDisplayIndeterminateProgressBar(( bool )e1. Tag, "加载中");
                };
            //当删除评论的时候需要刷新评论
            EventSingleton. Instance. OnRefreshComments += (s1, e1) =>
                {
                    //如果需要刷新的评论控件就是自身
                    if ( (s1 as CommentList) == this.comment_list )
                    {
                        this. comment_list. Reload(true, 0, 0, true);
                    }
                };
			if ( this.comment_list != null )
			{
				this. comment_list. GetTemplate(true);
			}
        }

        protected override void OnNavigatedTo(System. Windows. Navigation. NavigationEventArgs e)
        {
            if ( NextAppSingleton. Instance. IsReady )
            {
                //标志可以进行了
                this. IsEnabled = true;
				//this. catalog_list. Reload( );
				//this. posts_list. Reload( );
				//this. comment_list. Reload( );
				//this. posts_list. SelectPostListLayout( );
            }
            //检测是否登陆来判断 ApplicationBar
            Util. Tool. CheckAppBar(this);

            base. OnNavigatedTo(e);
        }

        #endregion

        #region Back 按键处理
        protected override void OnBackKeyPress(System. ComponentModel. CancelEventArgs e)
		{
			#region 处理关于我们的关闭
			if ( this.isAboutDisplay )
			{
				base. OnBackKeyPress(e);
				return;
			}
			#endregion
			if ( this. NavigationService. CanGoBack )
            {
                this. NavigationService. GoBack( );
            }
            else
            {
                if ( MessageBox. Show("是否确认退出程序", "温馨提示", MessageBoxButton. OKCancel) == MessageBoxResult. OK )
                {
                    //退出处理
                }
                else
                {
                    e. Cancel = true;
                }
            }

            base. OnBackKeyPress(e);
        }

        #endregion

        #region 底部的 ApplicationBar 相关事件
		private bool isAboutDisplay = false;
        private void menuItem_About_Click(object sender, EventArgs e)
        {
            //弹出关于我们提示框
            AboutPrompt about = new AboutPrompt
            {
                Title = Config. Instance. about_title,
				Footer = /*Config. Instance. about_copyright*/  "Powered by NextApp.cn",
                VersionNumber = /*Config. Instance. about_version*/ "版本 1.0",
                Body = new About( ),
            };
            about. Show( );
			isAboutDisplay = true;
			about. Completed += (s, e1) =>
				{
					this. isAboutDisplay = false;
				};
        }

        private void icon_Logout_Click(object sender, EventArgs e)
        {
            Util. Tool. Logout( );
            Util. Tool. CheckAppBar(this);
        }

        /// <summary>
        /// 刷新按钮点击事件
        /// </summary>
        private void icon_Reload_Click(object sender, EventArgs e)
        {
            switch ( this.pivot.SelectedIndex )
            {
                case 0:
                    this. catalog_list. Reload( );
                    break;
                case 1:
                    this. posts_list. Reload(true, 0, true);
                    break;
                case 2:
                    this. comment_list. Reload(true, 0, 0, true);
                    break;
            }
        }
        private void icon_Login_Click(object sender, EventArgs e)
        {
            Util. Tool. Login( );
        }
        private void icon_NewPost_Click(object sender, EventArgs e)
        {
            Util. Tool. AddPost_Click( );
        }
		private void menuItem_Setting_Click(object sender, EventArgs e)
		{
			this. NavigationService. Navigate(new Uri("/View/Setting.xaml", UriKind. Relative));
		}
        #endregion
    }
}