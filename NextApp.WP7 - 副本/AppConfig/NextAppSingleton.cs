﻿using System;
using System. IO. IsolatedStorage;
using System. Net;
using System. Xml. Linq;
using Microsoft. Phone. Shell;

namespace NextApp. WP7. AppConfig
{
    public sealed class NextAppSingleton
    {
        #region Singleton
        private static readonly NextAppSingleton instance = new NextAppSingleton( );
        public static NextAppSingleton Instance { get { return instance; } }
        private NextAppSingleton( ) 
        {
            this. Load( );
        }
        #endregion

        #region Initialize Method
        private void Load( )
        {
            WebClient client = new WebClient( );
            client. Headers[ "User-Agent" ] = Config. Instance. Http_User_Agent;
            client. DownloadStringCompleted += (s, e) =>
                {
                    if ( e.Error != null )
                    {
                        System. Diagnostics. Debug. WriteLine("加载API信息错误: " + e. Error. Message);
                        return;
                    }
					XElement x;
					try
					{
						x = XElement. Parse(e. Result);
					}
					catch ( Exception)
					{
						return;
					}
                    if ( x != null )
                    {
                        if ( x. Element("name") != null )
                            this. name = x. Element("name"). Value;
                        if ( x. Element("desc") != null )
                            this. desc = x. Element("desc"). Value;
                        if ( x. Element("home") != null )
                            this. home = x. Element("home"). Value;
                        XElement urls = x. Element("urls");
                        if ( urls != null )
                        {
                            this. url_catalog_list = urls. Element("catalog-list"). Value;
                            this. url_comment_delete = urls. Element("comment-delete"). Value;
                            this. url_comment_list = urls. Element("comment-list"). Value;
                            this. url_comment_pub = urls. Element("comment-pub"). Value;
                            this. url_login_validate = urls. Element("login-validate"). Value;
                            this. url_post_delete = urls. Element("post-delete"). Value;
                            this. url_post_detail = urls. Element("post-detail"). Value;
                            this. url_post_list = urls. Element("post-list"). Value;
                            this. url_post_pub = urls. Element("post-pub"). Value;
                            //记录
                            this. IsReady = true;
                            //触发事件 通知已经获取了API地址
                            if ( this.OnGetApiCompleted != null )
                            {
                                this. OnGetApiCompleted(this, EventArgs. Empty);
                            }
                        }
                    }
                };
            //异步获取
            client. DownloadStringAsync(new Uri(string. Format("{0}&guid={1}", Config. Instance. blog_ApiUrl, Guid. NewGuid( )), UriKind. Absolute));
        }

        #endregion

        #region Public Events
        /// <summary>
        /// API获取完毕的事件
        /// </summary>
        public event EventHandler OnGetApiCompleted;

        #endregion

        #region Public Properties

        /// <summary>
        /// API 是否已经获取完毕
        /// </summary>
        public bool IsReady
        {
            get 
            {
                return PhoneApplicationService. Current. State. ContainsKey("NextAppSingleton_IsReady") ?
                    ( bool )PhoneApplicationService. Current. State[ "NextAppSingleton_IsReady" ] : false;
            }
            private set 
            {
                PhoneApplicationService. Current. State[ "NextAppSingleton_IsReady" ] = value;
            }
        }

        /// <summary>
        /// API 名称
        /// </summary>
        public string name
        {
            get
            {
                return PhoneApplicationService. Current. State. ContainsKey("NextAppSingleton_name") ?
                    PhoneApplicationService. Current. State[ "NextAppSingleton_name" ]. ToString( ) : string. Empty;
            }
            private set
            {
                PhoneApplicationService. Current. State[ "NextAppSingleton_name" ] = value;
            }
        }

        /// <summary>
        /// API 描述
        /// </summary>
        public string desc
        {
            get
            {
                return PhoneApplicationService. Current. State. ContainsKey("NextAppSingleton_desc") ?
                    PhoneApplicationService. Current. State[ "NextAppSingleton_desc" ]. ToString( ) : string. Empty;
            }
            private set
            {
                PhoneApplicationService. Current. State[ "NextAppSingleton_desc" ] = value;
            }
        }

        /// <summary>
        /// API 提供方网站
        /// </summary>
        public string home
        {
            get
            {
                return PhoneApplicationService. Current. State. ContainsKey("NextAppSingleton_home") ?
                    PhoneApplicationService. Current. State[ "NextAppSingleton_home" ].ToString() : string. Empty;
            }
            private set
            {
                PhoneApplicationService. Current. State[ "NextAppSingleton_home" ] = value;
            }
        }

        /// <summary>
        /// API 发表文章
        /// </summary>
        public string url_post_pub
        {
            get 
            {
                return PhoneApplicationService. Current. State. ContainsKey("NextAppSingleton_url_post_pub") ?
                    PhoneApplicationService. Current. State[ "NextAppSingleton_url_post_pub" ]. ToString( ) : string. Empty;
            }
            private set 
            {
                PhoneApplicationService. Current. State[ "NextAppSingleton_url_post_pub" ] = value;
            }
        }

        /// <summary>
        /// API 删除文章
        /// </summary>
        public string url_post_delete
        {
            get
            {
                return PhoneApplicationService. Current. State. ContainsKey("NextAppSingleton_url_post_delete") ?
                    PhoneApplicationService. Current. State[ "NextAppSingleton_url_post_delete" ]. ToString( ) : string. Empty;
            }
            private set
            {
                PhoneApplicationService. Current. State[ "NextAppSingleton_url_post_delete" ] = value;
            }
        }

        /// <summary>
        /// API 发表评论
        /// </summary>
        public string url_comment_pub 
        {
            get
            {
                return PhoneApplicationService. Current. State. ContainsKey("NextAppSingleton_url_comment_pub") ?
                    PhoneApplicationService. Current. State[ "NextAppSingleton_url_comment_pub" ]. ToString( ) : string. Empty;
            }
            private set
            {
                PhoneApplicationService. Current. State[ "NextAppSingleton_url_comment_pub" ] = value;
            }
        }

        /// <summary>
        /// API 删除评论
        /// </summary>
        public string url_comment_delete
        {
            get
            {
                return PhoneApplicationService. Current. State. ContainsKey("NextAppSingleton_url_comment_delete") ?
                    PhoneApplicationService. Current. State[ "NextAppSingleton_url_comment_delete" ]. ToString( ) : string. Empty;
            }
            private set
            {
                PhoneApplicationService. Current. State[ "NextAppSingleton_url_comment_delete" ] = value;
            }
        }

        /// <summary>
        /// API 登陆
        /// </summary>
        public string url_login_validate 
        {
            get
            {
                return PhoneApplicationService. Current. State. ContainsKey("NextAppSingleton_url_login_validate") ?
                    PhoneApplicationService. Current. State[ "NextAppSingleton_url_login_validate" ]. ToString( ) : string. Empty;
            }
            private set
            {
                PhoneApplicationService. Current. State[ "NextAppSingleton_url_login_validate" ] = value;
            }
        }

        private string _url_post_list;
        /// <summary>
        /// API 获取文章
        /// </summary>
        public string url_post_list
        {
            get 
            {
                if ( string.IsNullOrWhiteSpace(this._url_post_list) )
                {
                    IsolatedStorageSettings. ApplicationSettings. TryGetValue("NextAppSingleton_url_post_list", out this. _url_post_list);
                }
                return this. _url_post_list;
            }
            set 
            {
                this. _url_post_list = value;
                IsolatedStorageSettings. ApplicationSettings[ "NextAppSingleton_url_post_list" ] = value;
                IsolatedStorageSettings. ApplicationSettings. Save( );
            }
        }

        private string _url_post_detail;
        /// <summary>
        /// API 获取单篇文章详情
        /// </summary>
        public string url_post_detail
        {
            get 
            {
                if ( string.IsNullOrWhiteSpace(this._url_post_detail) )
                {
                    IsolatedStorageSettings. ApplicationSettings. TryGetValue("NextAppSingleton_url_post_detail", out this. _url_post_detail);
                }
                return this. _url_post_detail;
            }
            set 
            {
                this. _url_post_detail = value;
                IsolatedStorageSettings. ApplicationSettings[ "NextAppSingleton_url_post_detail" ] = value;
                IsolatedStorageSettings. ApplicationSettings. Save( );
            }
        }

        private string _url_comment_list;
        public string url_comment_list
        {
            get 
            {
                if ( string.IsNullOrWhiteSpace(this._url_comment_list) )
                {
                    IsolatedStorageSettings. ApplicationSettings. TryGetValue("NextAppSingleton_url_comment_list", out this. _url_comment_list);
                }
                return this. _url_comment_list;
            }
            set 
            {
                this. _url_comment_list = value;
                IsolatedStorageSettings. ApplicationSettings[ "NextAppSingleton_url_comment_list" ] = value;
                IsolatedStorageSettings. ApplicationSettings. Save( );
            }
        }

        private string _url_catalog_list;
        /// <summary>
        /// API 获取文章分类
        /// </summary>
        public string url_catalog_list
        {
            get
            {
                if ( string.IsNullOrWhiteSpace(this._url_catalog_list) )
                {
                    IsolatedStorageSettings. ApplicationSettings. TryGetValue("NextAppSingleton_url_catalog_list", out this. _url_catalog_list);
                }
                return this. _url_catalog_list;
            }
            set 
            {
                this. _url_catalog_list = value;
                IsolatedStorageSettings. ApplicationSettings[ "NextAppSingleton_url_catalog_list" ] = value;
                IsolatedStorageSettings. ApplicationSettings. Save( );
            }
        }

        #endregion
    }
}
