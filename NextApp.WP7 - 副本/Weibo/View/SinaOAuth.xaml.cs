﻿using System;
using System. Windows;
using Microsoft. Phone. Controls;
using NextApp. WP7. Models;
using WeiboSdk;

namespace NextApp. WP7. Weibo. View
{
    public partial class SinaOAuth : PhoneApplicationPage
    {
        public SinaOAuth( )
        {
            SdkData. AppKey = "156524751";
            SdkData. AppSecret = "b09a216632a74487884de5d16de8fb11";

            SdkData. AesKey = "12345678901234567890123456789012";
            SdkData. AesIV = "1234567890123456";
            InitializeComponent( );
            //OAuthControl初始化
            this. OAuthControlInitialize( );
        }
        private void OAuthControlInitialize( )
        {
            //保存密码
            this. oauthControl. ifSave = true;
            //
            this. oauthControl. OAuthBack = loginBack;
            //
            this. oauthControl. OAuthBrowserNavigating = new EventHandler(oauthBrowserNavigating);
            //
            this. oauthControl. OAuthBrowserNavigated = new EventHandler(oauthBrowserNavigated);
        }

        #region 微博登陆处理
        private void loginBack(bool isSucess, SdkAuthError err, SdkAuthRes response)
        {
            if ( err. errCode == SdkErrCode. SUCCESS )
            {
                System. Diagnostics. Debug. WriteLine("UID: {0}   AccessToken: {1}   AccessTokenSecret: {2}",
                                                                                    response. userId, response. acessToken, response. acessTokenSecret);
                //可自行保存
                //SinaUtil. Instance. AccessToken = response. acessToken;
                //SinaUtil. Instance. AccessTokenSecret = response. acessTokenSecret;
                //SinaUtil. Instance. UID = response. userId;
                //页面跳转
                //this. NavigationService. Navigate(new Uri("/Weibo/View/SinaPage.xaml", UriKind. Relative));
                SdkShare share = new SdkShare { Message = string. Format("{0}  {1}", DataSingleton. Instance. postToShare. title, DataSingleton. Instance. postToShare. url) };
                share. Completed = new EventHandler<SendCompletedEventArgs>(this. ShareWeiboCompleted);
                share. Show( );
            }
                //如果登陆失败
            else
            {
                switch ( err.errCode )
                {
                    case SdkErrCode. AUTH_FAILED:
                        System. Diagnostics. Debug. WriteLine("登陆失败,  错误码: {0}", err. specificCode);
                        break;
                    case SdkErrCode. NET_UNUSUAL:
                        System. Diagnostics. Debug. WriteLine("网络异常状况");
                        break;
                    case SdkErrCode. SERVER_ERR:
                        System. Diagnostics. Debug. WriteLine("访问服务器失败: 错误码: {0}", err. specificCode);
                        break;
                    case SdkErrCode. XPARAM_ERR:
                        break;
                }
            }
        }

        private void ShareWeiboCompleted(object sender, SendCompletedEventArgs e)
        {
            if ( e. IsSendSuccess )
            {
                MessageBox. Show("发送成功");

                #region 废弃的代码
                //if ( this. NavigationService. CanGoBack )
                //{
                //    this. NavigationService. GoBack( );
                //}
                #endregion
            }
            else
            {
                MessageBox. Show("发送分享失败");
            }

            #region 跳转到刚才看的文章页
            this. NavigationService. Navigate(new Uri(string. Format("/View/PostDetail.xaml?post={0}&portal={1}",
                                                                        DataSingleton. Instance. postToShare. id, DataSingleton. Instance. GoToSharePageType), UriKind. Relative));
            #endregion

            #region 取消的代码
            //switch ( e. ErrorCode )
            //{
            //    case SdkErrCode. AUTH_FAILED:
            //        System. Diagnostics. Debug. WriteLine("验证失败 错误: {0}", e. ErrorCode);
            //        break;
            //    case SdkErrCode. NET_UNUSUAL:
            //        System. Diagnostics. Debug. WriteLine("网络异常  错误: {0}", e. ErrorCode);
            //        break;
            //    case SdkErrCode. SERVER_ERR:
            //        System. Diagnostics. Debug. WriteLine("服务器异常 错误: {0}", e. ErrorCode);
            //        break;
            //    case SdkErrCode. SUCCESS:
            //        System. Diagnostics. Debug. WriteLine("分享成功");
            //        break;
            //    case SdkErrCode. XPARAM_ERR:
            //        break;
            //    default:
            //        break;
            //}
            #endregion
        }

        private void oauthBrowserNavigating(object sender, EventArgs e)
        {
            
        }

        private void oauthBrowserNavigated(object sender, EventArgs e)
        {

        }
        #endregion
    }
}