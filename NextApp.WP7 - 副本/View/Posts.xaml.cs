﻿using System;
using System. Linq;
using System. Windows;
using Microsoft. Phone. Controls;
using NextApp. WP7. AppConfig;
using NextApp. WP7. Models;
using NextApp. WP7. Util;

namespace NextApp. WP7. View
{
    public partial class Posts : PhoneApplicationPage
    {
        #region Initialize
        public Posts( )
        {
            InitializeComponent( );
			this. PrepareToOrientation( );
            this. ApplicationTitle. Text = Config. Instance. blog_title;
        }

        protected override void OnNavigatedTo(System. Windows. Navigation. NavigationEventArgs e)
        {
            //登陆菜单初始化
            Util. Tool. CheckAppBar(this);

            int catalogID = NavigationContext. QueryString. ContainsKey("catalog") ?
                NavigationContext. QueryString[ "catalog" ]. ToInt32( ) :
                0;
            this. PageTitle. Text = NavigationContext. QueryString. ContainsKey("title") ? NavigationContext. QueryString[ "title" ] : "所有文章";
            this. postsList. catalogID = catalogID;
			//if ( this.postsList.postsBinding.Count <= 0 )
			//{
			//    this. postsList. Reload( );
			//}

			//this. postsList. Reload( );

            base. OnNavigatedTo(e);
        }
        #endregion

        #region 导向发表文章页面与回退处理
        private void icon_NewPost_Click(object sender, EventArgs e)
        {
            Util. Tool. AddPost_Click( );
        }

        private void icon_GoBack_Click(object sender, EventArgs e)
        {
            if ( this. NavigationService. CanGoBack )
            {
                this. NavigationService. GoBack( );
            }
            else
            {
                this. NavigationService. Navigate(new Uri("/MainPage.xaml", UriKind. Relative));
            }
        }
        #endregion

        #region 将文章订到桌面上
        private void menuItem_PinToStart_Click(object sender, EventArgs e)
        {
            if ( DataSingleton. Instance. catalogs != null )
            {
                Catalog c = DataSingleton. Instance. catalogs. FirstOrDefault(ca => ca. id == this. postsList. catalogID);
                if ( c != null )
                {
                    Util. Tool. CreateShellTile(c);
                }
            }
            else
            {
                MessageBox. Show("此文章分类已经被订到桌面上了");
            }
        }
        #endregion

        #region 登入与注销
        //登入
        private void menuItem_Login_Click(object sender, EventArgs e)
        {
            Util. Tool. Login( );
        }

        //注销
        private void menuItem_Logout_Click(object sender, EventArgs e)
        {
            Util. Tool. Logout( );
            Util. Tool. CheckAppBar(this);
        }
        #endregion
    }
}