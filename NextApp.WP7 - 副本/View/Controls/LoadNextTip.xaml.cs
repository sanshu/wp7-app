﻿using System;
using System. Collections. Generic;
using System. Linq;
using System. Net;
using System. Windows;
using System. Windows. Controls;
using System. Windows. Documents;
using System. Windows. Input;
using System. Windows. Media;
using System. Windows. Media. Animation;
using System. Windows. Shapes;
using Microsoft. Phone. Shell;
using Microsoft. Phone. Controls;

namespace NextApp. WP7. View. Controls
{
	public partial class LoadNextTip : UserControl
	{
		public LoadNextTip( )
		{
			InitializeComponent( );
			this. MouseLeftButtonDown += (s, e) =>
				{
					if ( this.Click != null )
					{
						this. Click(this, null);
					}
				};
			this. Loaded += new RoutedEventHandler(LoadNextTip_Loaded);
		}

		void LoadNextTip_Loaded(object sender, RoutedEventArgs e)
		{
			( Application. Current. RootVisual as PhoneApplicationFrame ). OrientationChanged += (s1, e1) =>
			{
				//处理宽度
				switch ( e1. Orientation )
				{
					case PageOrientation. Landscape:
					case PageOrientation. LandscapeLeft:
					case PageOrientation. LandscapeRight:
						this. Width = this. MinWidth = 790;
						break;
					case PageOrientation. Portrait:
					case PageOrientation. PortraitDown:
					case PageOrientation. PortraitUp:
						this. Width = this. MinWidth = 480;
						break;
				}
			};
		}

		public event EventHandler Click;

		public string Text
		{
			get { return this. tblock_Tip. Text; }
			set 
			{
				this. tblock_Tip. Text = value;
			}
		}
	}
}
